### KNNの結果
# true class
![true class](./img/true_class.png)
# k=2
![k=2](./img/anim_2.gif)
# k=3
![k=3](./img/anim_3.gif)
# k=4
![k=4](./img/anim_4.gif)
# k=5
![k=5](./img/anim_5.gif)
