# coding: UTF-8
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.animation as animation


class PCA:
    def __init__(self, n_dim=2):
        self.n_dim = n_dim

    def fit(self, X, n_dim=2):
        cov = np.cov(X, rowvar=False)
        self.vec = np.linalg.eig(cov)[1][:n_dim].T

    def projection(self, X):
        return np.dot(X, self.vec)


def main():
    features = ['sepal_length', 'sepal_width', 'petal_length', 'petal_width']
    label = 'species'
    class_label = ['setosa', 'versicolor', 'virginica']
    k = 2
    n_steps = 15

    df = pd.read_csv('iris.csv')
    X = df[features].values
    y = df[label].map(class_label.index).values
    centroids = np.random.randn(k, len(features))*0.1 + np.mean(X, axis=0)

    fig = plt.figure(figsize=(12, 7))
    axs_features = [[0, 1], [0, 2], [0, 3], [1, 2], [1, 3], [2, 3]]
    axs = [fig.add_subplot('23{}'.format(i)) for i in range(6)]
    for i in range(6):
        axs[i].set_xlabel(features[axs_features[i][0]])
        axs[i].set_ylabel(features[axs_features[i][1]])
    plt.subplots_adjust(wspace=0.4, hspace=0.3)

    ims = []
    colors = ['red', 'orange', 'dodgerblue', 'darkviolet', 'fuchsia']
    for step in range(n_steps):
        distances = np.stack(
            [np.linalg.norm(X - centroids[i], axis=1) for i in range(k)])
        classes = np.argmin(distances, axis=0)

        imk = []
        for i in range(k):
            for j in range(6):
                im = axs[j].scatter(X[classes == i, axs_features[j][0]],
                                    X[classes == i, axs_features[j][1]], color=colors[i], alpha=0.3)
                imk.append(im)
                im,  = axs[j].plot(centroids[i, axs_features[j][0]], centroids[i, axs_features[j][1]],
                                   marker='*', markersize=10, c=colors[i])
                imk.append(im)
        ims.append(imk)

        for i in range(k):
            if len(X[classes == i]) != 0:
                centroids[i] = np.mean(X[classes == i], axis=0)

    ani = animation.ArtistAnimation(fig, ims, interval=300, blit=False)
    ani.save('anim_{}.gif'.format(k), writer="imagemagick", dpi=144)


if __name__ == '__main__':
    main()
