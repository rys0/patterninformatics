# coding: UTF-8
import collections

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


class KNN:
    def __init__(self, k):
        self.X = None
        self.y = None
        self.distance = self.euclid_distance
        self.set_k(k)

    def set_k(self, k):
        self.k = k

    def fit(self, X, y):
        self.X = X
        self.y = y

    def clear(self):
        self.X = None
        self.y = None

    def euclid_distance(self, x1, x2):
        return np.linalg.norm(x1-x2)

    def predict(self, x):
        if self.X is None:
            raise ValueError('Use fit(X, y) to set dataset before predicting.')

        X_dist = np.apply_along_axis(
            lambda x2: self.distance(x, x2), 1, self.X)
        k_nearest = np.argpartition(X_dist, self.k)[:self.k]
        c = collections.Counter(self.y[k_nearest])
        return c.most_common(1)[0][0]

    def predict_batch(self, X):
        return np.array([self.predict(x) for x in X])


def cross_validation_generator(X, y, k):
    n_samples = len(X)
    block_size = n_samples // k
    idx = np.random.permutation(n_samples)
    for i in range(k):
        if i == k-1:
            X_train = X[:i*block_size]
            y_train = y[:i*block_size]
            X_test = X[i*block_size:]
            y_test = y[i*block_size:]
        else:
            X_train = np.concatenate(
                [X[:i*block_size], X[(i+1)*block_size:]], axis=0)
            y_train = np.concatenate(
                [y[:i*block_size], y[(i+1)*block_size:]], axis=0)
            X_test = X[i*block_size:(i+1)*block_size]
            y_test = y[i*block_size:(i+1)*block_size]
        yield X_train, y_train, X_test, y_test


def main():
    features = ['sepal_length', 'sepal_width', 'petal_length', 'petal_width']
    label = 'species'
    class_label = ['setosa', 'versicolor', 'virginica']
    k_max = 30
    k_fold = 5

    df = pd.read_csv('iris.csv')
    X = df[features].values
    y = df[label].map(class_label.index).values

    accuracy = np.empty((k_max, k_fold))
    fold_i = 0
    for X_train, y_train, X_test, y_test in cross_validation_generator(X, y, k_fold):
        for k in range(k_max):
            classifier = KNN(k+1)
            classifier.fit(X_train, y_train)
            y = classifier.predict_batch(X_test)
            accuracy[k][fold_i] = np.sum(y == y_test)/float(len(y_test))
            classifier.clear()

        fold_i += 1

    print('accuracies ({}-fold cross validation)'.format(k_fold))
    means = np.mean(accuracy, axis=1)
    stds = np.std(accuracy, axis=1)
    for k in range(k_max):
        print('k={}: {:.3f}±{:.3f}'.format(
            k+1, np.mean(accuracy[k]), np.std(accuracy[k])))

    plt.figure(figsize=(10, 7))
    plt.errorbar(np.arange(1, k_max+1), means, yerr=stds,
                 marker='o', capthick=1, capsize=8, lw=1)
    plt.xlabel('k')
    plt.ylabel('accuracy')
    plt.savefig('knn_result.pdf')


if __name__ == '__main__':
    main()
