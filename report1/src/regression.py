# coding: UTF-8
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import plotly.graph_objects as go
import chart_studio.plotly as py
import plotly.io as pio


class LinearRegression:
    def __init__(self, n_features):
        self.W = np.random.randn(n_features) / np.sqrt(n_features)

    def fit_normal(self, X, y):
        xtx_inv = np.linalg.pinv(np.dot(X.T, X))
        self.W = np.dot(np.dot(xtx_inv, X.T), y)

    def predict(self, X):
        return np.dot(X, self.W)


def cross_validation_generator(X, y, k):
    n_samples = len(X)
    block_size = n_samples // k
    idx = np.random.permutation(n_samples)
    for i in range(k):
        if i == k-1:
            X_train = X[:i*block_size]
            y_train = y[:i*block_size]
            X_test = X[i*block_size:]
            y_test = y[i*block_size:]
        else:
            X_train = np.concatenate(
                [X[:i*block_size], X[(i+1)*block_size:]], axis=0)
            y_train = np.concatenate(
                [y[:i*block_size], y[(i+1)*block_size:]], axis=0)
            X_test = X[i*block_size:(i+1)*block_size]
            y_test = y[i*block_size:(i+1)*block_size]
        yield X_train, y_train, X_test, y_test


class Scaler:
    def fit(self, X):
        self.mean = np.mean(X, axis=0)
        self.std = np.std(X, axis=0)
        # don't normalize dummy feature
        self.mean[-1] = 0
        self.std[-1] = 1

    def scale(self, X):
        return (X - self.mean) / self.std

    def rev_scale(self, X):
        return X * self.std + self.mean


def plot_predictor(scaler, reg, X_data, y_data, features):
    N = 100
    r = scaler.mean + scaler.std
    x1 = np.linspace(0, r[0]*2, N)
    x2 = np.linspace(0, r[1]*2, N)
    X1, X2 = np.meshgrid(x1, x2)
    X3 = np.ones(len(X1.ravel()))
    X = np.stack([X1.ravel(), X2.ravel(), X3], axis=-1)
    y = reg.predict(scaler.scale(X))
    surf = go.Surface(x=X1, y=X2, z=y.reshape(X1.shape), colorscale='Blues')
    scat = go.Scatter3d(
        x=X_data[:, 0], y=X_data[:, 1], z=y_data, mode='markers', marker={"size": 2})
    fig = go.Figure(data=[scat, surf])
    camera = dict(
        up=dict(x=0, y=0, z=1),
        center=dict(x=0, y=0, z=0),
        eye=dict(x=1.7, y=1.7, z=1.1)
    )
    fig.update_layout(scene=dict(
                      xaxis_title=features[0],
                      yaxis_title=features[1],
                      zaxis_title='mpg',
                      camera=camera))
    pio.write_image(fig, 'regression_result.pdf', width=700, height=775)


def main():
    column_names = ['mpg', 'cylinders', 'displacement', 'horsepower',
                    'weight', 'acceleration', 'model year', 'origin', 'car name']
    df = pd.read_csv('auto-mpg.data', names=column_names,
                     na_values="?", comment='\t',
                     sep=" ", skipinitialspace=True)
    features = ['horsepower', 'weight']
    df = df.dropna(subset=features)
    X = df[features].values
    X = np.concatenate([X, np.ones(len(X))[:, np.newaxis]], axis=1)
    y = df['mpg'].values
    reg = LinearRegression(X.shape[1])
    k = 5
    loss = []
    scaler = Scaler()
    for X_train, y_train, X_test, y_test in cross_validation_generator(X, y, k):
        scaler.fit(X_train)
        reg.fit_normal(scaler.scale(X_train), y_train)
        y_pred = reg.predict(scaler.scale(X_test))
        loss.append(np.sum((y_pred-y_test)**2)/len(y_pred))
    print('test loss = {}±{}'.format(np.mean(loss), np.std(loss)))
    reg.fit_normal(scaler.scale(X), y)
    plot_predictor(scaler, reg, X, y, features)


if __name__ == '__main__':
    main()
